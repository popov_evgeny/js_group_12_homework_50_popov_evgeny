import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmsBlockComponent } from './films-block.component';

describe('FilmsBlockComponent', () => {
  let component: FilmsBlockComponent;
  let fixture: ComponentFixture<FilmsBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilmsBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmsBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
