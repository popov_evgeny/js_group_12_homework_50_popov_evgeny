import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { ContainerComponent } from './container/container.component';
import { HeaderComponent } from './header/header.component';
import { ContentBlockComponent } from './content-block/content-block.component';
import { MainComponent } from './main/main.component';
import { FilmsBlockComponent } from './films-block/films-block.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    ContainerComponent,
    HeaderComponent,
    ContentBlockComponent,
    MainComponent,
    FilmsBlockComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
